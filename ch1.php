<?php

/**
Uncomment the print statements to view all the steps
*/

//$str_in = "abbababbabkeleeklkel";				/* bae */
//$str_in = "aaaaffffdddddggggbbeeeeeeeea";		/* eadfg */
//$str_in = "aaaaggggdddddffffbbeeeeeeeea";		/* eadfg */
$str_in = "baaac";							/* NONE */



echo "</br>Output : </br>". challenge1($str_in);

function challenge1($str_in){
	
	echo "Given String : </br>";
	var_dump($str_in);
	echo "</br>";
	
	$arr_in = str_split($str_in);
	/*...spiltting the string into array of characters */
		 	
	$arr_inter = array_count_values($arr_in);
	///*...getting the count of each character */
	//echo "</br>getting the count of each character : </br>";
	//print_r($arr_inter);
	//echo "</br>"; 
	
	$arr_inter2 = array_unique($arr_inter);
	/*... getting unique values of the counts */
	//echo "</br>unique values of the counts : </br>";
	//print_r($arr_inter2);
	//echo "</br>";
	
	arsort($arr_inter2);
	//...getting descending order of unique values of the counts
	//echo "</br>descending order of unique values of the counts : </br>";
	//print_r($arr_inter2);
	//echo "</br>";	
	
	$str_out = "NONE";
	$arr_out = array();
	foreach($arr_inter2 as $count){
		if($count < 4 && $str_out == 'NONE'){
			/* return for NONE if the intermediate array $arr_inter has less than 3 count at first possition */
			return $str_out;
		}elseif($count < 4){
			/* for ending neglecting the lower counts */
			break;
		}else{
			$str_out = 'Exist';
			//echo "</br>";
			//echo $count;
			
			$temp = array_keys($arr_inter , $count);
			/* choose character of this count */
			//echo "</br>choose character of this count :</br>";
			//print_r($temp);
			//echo "</br>";
			
			asort($temp);
			/*...getting an array in assending order of ASCII value */
			//echo "</br>getting an array  in assending order of ASCII value : </br>";			
			//print_r($temp);
			//echo "</br>";			
			
			$arr_out = array_merge($arr_out, $temp);
			/*...make an array of required characters for output */
		}		
	}
	
	/*...Output array in descending order of counts and assending order of ASCII value */
	//echo "</br>Output array in descending order of counts and assending order of ASCII value : </br>";			
	//print_r($arr_out);
	//echo "</br>";
	
	$str_out = implode("", $arr_out);
	return $str_out;
}

?>