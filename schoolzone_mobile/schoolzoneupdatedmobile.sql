-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2016 at 04:06 PM
-- Server version: 5.5.49-log
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schoolzoneupdatedmobile`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(250) NOT NULL,
  `eventTitle` varchar(250) NOT NULL,
  `eventDescription` text,
  `eventFor` varchar(10) DEFAULT NULL,
  `enentPlace` varchar(250) DEFAULT NULL,
  `eventDate` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `eventTitle`, `eventDescription`, `eventFor`, `enentPlace`, `eventDate`) VALUES
(1, 'Sports Day - 16th September 2016', '&lt;p&gt;As per annual schedule, we are ...&lt;/p&gt;\n', 'all', 'Ground', '1473984000'),
(2, 'Alumni Day', '&lt;p&gt;Alumni Day has been scheduled for 3rd December 2016&lt;/p&gt;\n', 'all', 'Hall', '1480723200');

-- --------------------------------------------------------

--
-- Table structure for table `newsboard`
--

CREATE TABLE IF NOT EXISTS `newsboard` (
  `id` int(250) NOT NULL,
  `newsTitle` varchar(250) NOT NULL,
  `newsText` text NOT NULL,
  `newsFor` varchar(250) NOT NULL,
  `newsDate` int(250) NOT NULL,
  `creationDate` int(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsboard`
--

INSERT INTO `newsboard` (`id`, `newsTitle`, `newsText`, `newsFor`, `newsDate`, `creationDate`) VALUES
(1, 'Holiday on 26th August, 2016', '&lt;p&gt;On the occasion of Dahi Handi.....&lt;/p&gt;\n', 'all', 1472169600, 1471340248),
(2, 'SIWS Library - website', '&lt;p&gt;SIWS Library - website selected as amongst top 5 best websites in Mumbai district area&lt;/p&gt;\n', 'all', 1471478400, 1471521537);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsboard`
--
ALTER TABLE `newsboard`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `newsboard`
--
ALTER TABLE `newsboard`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
