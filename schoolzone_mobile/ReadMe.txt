1. Install WAMP on your windows machine in C drive.
2. Run the wamp.exe file from the desktop shortcut OR bin
3. Copy the folder "schoolzone_mobile" to the path "C:\wamp\www".

4. Try using URL "http://localhost:8484/schoolzone_mobile/"
5. This should show "WELCOME" message

6. Try using URL "http://localhost:8484/schoolzone_mobile/dbtest.php"
7. Database can be access using "http://localhost:8484/phpMyAdmin"

8. Create a new DB with the name as specify in the file "dbtet.php"
9. Try using URL "http://localhost:8484/schoolzone_mobile/dbtest.php"
10. This should show "connected successfully"

11. Import the given file "schoolzoneupdatedmobile.sql" to the newly created DataBase using phpMyAdmin.
12. Try using URL "http://localhost:8484/schoolzone_mobile/notifications.php?news_id=1&event_id=1"
13. This should give a response containg status as "success"

You are done with the server side setup..!!