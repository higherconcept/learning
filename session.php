<?php
// Start the session
session_start();

// Set session variables
$_SESSION["name"] = "Nilam";
$_SESSION["middlename"] = "Virappa";
$_SESSION["surname"] = "Matwadkar";
echo "Session variables are set.</br>";
print_r($_SESSION);

unset($_SESSION["name"]);
echo "</br>Session variables are set.</br>";
print_r($_SESSION);
session_unset(); 
echo "</br>Session variables are set.</br>";
print_r($_SESSION);
?>